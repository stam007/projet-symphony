<?php

namespace App\Form;

use App\Entity\Reservation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\DateIntervalType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


//reservation form qui va appercu dans la page reservation on l'ajouter leur classes pour la mise en form

class ReservationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('start',DateType:: class,['attr'=>['class'=>'form-control','widget' => 'choice', ]])
            ->add('stop',DateType::class,['attr'=>['class'=>'form-control']])
            ->add('mobile',TextType::class,['attr'=>['class'=>'form-control','placeholder'=>'Téléphone']])
            ->add('email',TextType::class,['attr'=>['class'=>'form-control','placeholder'=>'Email']])
            ->add('country',TextType::class,['attr'=>['class'=>'form-control','placeholder'=>'Ville']])
            ->add('name',TextType::class,['attr'=>['class'=>'form-control','placeholder'=>'Nom et Prénom']])
            //            ->add('vehicle')
            ->add('save',SubmitType::class,array(
                'label'=>'Reserver',
                'attr'=>array('class'=> 'btn btn-primary')
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Reservation::class,
        ]);
    }
}
