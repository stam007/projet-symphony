<?php

namespace App\Form;

use App\Entity\Contact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\{TextType,ButtonType,EmailType,HiddenType,PasswordType,TextareaType,SubmitType,NumberType,DateType,MoneyType,BirthdayType};



//contact form qui va appercu dans la page contact on l'ajouter leur classes pour la mise en form
class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom',TextType::class,['attr'=>['class'=>'form-control']])
            ->add('email',EmailType::class,['attr'=>['class'=>'form-control']])
            ->add('message',TextareaType::class,['attr'=>['class'=>'form-control']])

            ->add('save',SubmitType::class,array(
                'label'=>'Envoyer',
                'attr'=>array('class'=> 'btn btn-primary')
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Contact::class,
        ]);
    }
}
