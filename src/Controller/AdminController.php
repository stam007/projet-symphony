<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

use App\Entity\Picture;
use App\Entity\Vehicle;
use App\Repository\VehicleRepository;
use App\Repository\PictureRepository;

class AdminController extends AbstractController
{
    /**
     * @Route("/addcar", name="addcar", methods="POST")
     */
    public function index(Request $request)//cette fonction est pour ajouter un nouveau voiture par l'admin dans la base de donnees
    {
        $entityManager = $this->getDoctrine()->getManager();// creation de entity instance pour gerre l'eregistrement dans la base de donnees
        $vehicle = new Vehicle();// creation de nouveau instance de entity voiture 
        $vehicle->setName($request->get('name'));// prendre la nom de voiture de la request formulaire de l'admin
        $vehicle->setYear($request->get('year'));// prendre l'annees de voiture de la request formulaire de l'admin
        $vehicle->setMarque($request->get('marque'));// prendre la marque de voiture de la request formulaire de l'admin
        $vehicle->setPrice($request->get('price'));// prendre la prix de reservation  de voiture de la request formulaire de l'admin
        $vehicle->setAvailable($request->get('available'));// prendre la disponibilite de voiture de la request formulaire de l'admin
        $vehicle->setDescription($request->get('description'));// prendre la description de voiture de la request formulaire de l'admin

        // Count total files
        $countfiles = count($_FILES['pictures']['name']);//prendre le nombre des images de voiture 

        // Looping all files
        for ($i = 0; $i < $countfiles; $i++) {
            $filename = $_FILES['pictures']['name'][$i];//prend le nom de l'image
            if ($filename) {//si existe
                // Upload file
                move_uploaded_file(
                    $_FILES['pictures']['tmp_name'][$i],
                    'upload/' . $filename
                );//eregistrement de l'image dans le path public/upload

                $picture = new Picture();// creation de la nouvelle instance de image 
                $picture->setPath('upload/' . $filename);//ajoute a l'instance le path de l'image

                $vehicle->addPicture($picture);//on associe le voiture avec ses images
                $entityManager->persist($picture);//enregistrement de image 
            }
        }

        $entityManager->persist($vehicle);// enregistrement de voiture qui deja en association avec leur images

        $entityManager->flush();//mettre a jour la base de donnees

        return new Response('save this vehicle ' . $vehicle->getId());

    }


     /**
     * @Route("/deleteimg/{id}",name="deleteimg")
     */
    public function renderDeleteImage($id, PictureRepository $picture)//cette fonction est pour supprimer les images qui il sont associes par un voiture // on mettre ajour les voiture par l'admin
    {
        $entityManager = $this->getDoctrine()->getManager();//creation de nouveau instance de l'entity manager qui va mettre les echange dans la base de donnees

        $img = $picture->find($id);//on obtient l'image par leur id
        $entityManager->remove($img);//supprission de l'image 
        $entityManager->flush();// mettre a jour la base de donnees

        return new Response('deleted');//message de confiration de supprission
    }
    /**
     * @Route("/delete/{id}",name="delete")
     */
    public function renderDeleteVehicle($id, VehicleRepository $vehicle)//cette fonction est quand l'admin decide de supprimer un voiture
    {
        $entityManager = $this->getDoctrine()->getManager();//creation de nouveau instance de l'entity manager qui va mettre les echange dans la base de donnees

        $article = $vehicle->find($id);//on obtient la voiture par sa id
        $entityManager->remove($article);//supprission de la voiture 
        $entityManager->flush();//mettre a jour la base de donnees

        return $this->redirect('/');// cette redirection pour que l'admin ne voir plus la voiture qui il est supprimee //refresh de la page
    }

    /**
     * @Route("/update/{id}",name="update", methods="POST")
     */
    public function renderUpdateVehicle(
        $id,
        VehicleRepository $article,
        Request $request
    ) //cette fonction pour mettre a jour un voiture par l'admin elle contient comme des parametre l'id de la voiture est les nouveau attributs de cette voiture 
    {
        $entityManager = $this->getDoctrine()->getManager();//creation de nouveau instance de l'entity manager qui va mettre les echange dans la base de donnees

        $vehicle = $article->find($id);//on obtient la voiture par sa id
        $vehicle->setName($request->get('name'));//on obtient les nouveau echange de name de la voiture 
        $vehicle->setYear($request->get('year'));//on obtient les nouveau echange de l'annees de la voiture 
        $vehicle->setMarque($request->get('marque'));//on obtient les nouveau echange de la marque de la voiture 
        $vehicle->setPrice($request->get('price'));//on obtient les nouveau echange de la prix de la voiture 
        $vehicle->setAvailable($request->get('available'));//on obtient les nouveau echange de la disponibilite de la voiture 
        $vehicle->setDescription($request->get('description'));//on obtient les nouveau echange de la description de la voiture 

        // Count total files
        $countfiles = count($_FILES['pictures']['name']);// le noumbre des nouveau image de la voiture 

        // Looping all files
        for ($i = 0; $i < $countfiles; $i++) {
            $filename = $_FILES['pictures']['name'][$i];//prend le nom de l'image
            if ($filename) {
                // Upload file
                move_uploaded_file(
                    $_FILES['pictures']['tmp_name'][$i],
                    'upload/' . $filename
                );//eregistrement de l'image dans le path public/upload

                $picture = new Picture();//create des nouveau instance entity pour chaque image
                $picture->setPath('upload/' . $filename);//enrigistres les images par leur path 

                $vehicle->addPicture($picture);//associes les images a cette voiture 
                $entityManager->persist($picture);//enrigistres les nouveau images
            }
        }

        $entityManager->flush();//mettre ajour la base de donnees

        return new Response('save this vehicle ' . $vehicle->getId());//return message de confirmation
    }

    /**
     * @Route("/getbyid/{id}",name="getbyid")
     */
    public function renderGetByIdVehicle($id, VehicleRepository $vehicle)//cette fonction return les voiture par leur id /// cette fonction est appeler par le code ajax
    {
        $entityManager = $this->getDoctrine()->getManager();//creation de nouveau instance de l'entity manager qui va mettre les echange dans la base de donnees

        $article = $vehicle->find($id);//obtient la voiture par leur id
        $pict = array();//tableau qui va contenir les images correspondant a cette voiture 
        foreach ($article->getpictures() as $value) {
            $pict[$value->getid()] = [
                'id' => $value->getid(),
                'path' => $value->getpath()
            ];
        }//associe a le tableau pict les path des images

        $response = new JsonResponse([
            'name' => $article->getname(),
            'id' => $article->getid(),
            'marque' => $article->getid(),
            'description' => $article->getdescription(),
            'price' => $article->getprice(),
            'year' => $article->getyear(),
            'available' => $article->getavailable(),
            'pictures' => $pict
        ]);//return la voiture avec ses images associes en form json qui va le ajax bien manipuler pour dans le cas de modification de la voiture par l'admin 

        return $response;
    }
}
