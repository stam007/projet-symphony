<?php
// src/Controller/LuckyController.php
namespace App\Controller;

use App\Entity\Contact;
use App\Entity\Reservation;
use App\Entity\Vehicle;
use App\Form\ContactType;
use App\Form\ReservationType;
use App\Repository\PictureRepository;
use App\Repository\VehicleRepository;
use Doctrine\ORM\EntityManagerInterface;
use phpDocumentor\Reflection\Types\String_;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Knp\Component\Pager\PaginatorInterface;


use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\{
    TextType,
    ButtonType,
    EmailType,
    HiddenType,
    PasswordType,
    TextareaType,
    SubmitType,
    NumberType,
    DateType,
    MoneyType,
    BirthdayType
};
use Twig\Loader\ArrayLoader;
use function Sodium\add;

class DefaultController extends AbstractController
{
    private $session;

    public function __construct(SessionInterface $session)//creation une session
    {
        $this->session = $session;
    }

    /**
     * @Route("/", methods="GET|POST",name="home")
     */
    public function home(VehicleRepository $vehiclerepository, PaginatorInterface $paginator,Request $request,EntityManagerInterface $em)//cette fonction est pour les deux cas possible si l'utilisateur faire un filter dans la page home et le dexieme c'est return de default page d'acceuile  avec la form de la recherche
    {
         $result= $vehiclerepository->findAll();//obtenir tous les voiture disponibles
        $home_data = array_slice($result, 0, 6);//on besoin just de 6 voiture pour l'affichier dans la page d'acceiule
        $defaultData = ['message' => 'Type your message here'];
        $form=$this->createFormBuilder($defaultData)// creation de formbuilder qui va gerres les input de la filter de recherche 

             ->add('save',SubmitType::class,array(
                 'label'=>'Rechercher',
                 'attr'=>array('class'=> 'btn btn-secondary btn-lg ml-lg-3')
             ))
             ->getForm();//ajouter les classes pour les mise en form cette form

        $form->handleRequest($request);//en rendre cette form peut gerrer les request

        if($form->isSubmitted()  )//si l'utilisateur appuier sur le button recherche de la filter
        {



            if($request->get('Model')  != null){
                $data['name']=$request->get('Model') ;
            }
            if($request->get('Prix')  != null){
                $data['price']= $request->get('Prix') ;
            }
            if($request->get('Marque')  != null){
                $data[    'marque']= $request->get('Marque') ;
            }
            if($request->get('Annee')  != null){
                $data[    'year' ]= $request->get('Annee') ;
            }

            $criteria = array_filter($data);//on obtient les critere de rechere par l'utilisateur 

            $result = $vehiclerepository->findBy($criteria);//et on onbtient les voiture qui ils contient cette critere 
       




            $vehicule_grille= $paginator->paginate(

                $result,

                $request->query->getInt('page', 1),

                5
            );///creation une pagination qui par defaut return un seule page contien 5 voitures
            return $this->render('inventory-grid.html.twig', [
                'vehicule_grille' => $vehicule_grille
            ]);//return les voitures avec la page qui va afficher le en grid layout 

        }

        return $this->render('home.html.twig', ['home_data' => $home_data, 'form' => $form->createView()]);// sinon return de la page home avec 6 voiture 
    }

    /**
     * @Route("/blog",name="blog")
     */
    public function renderBlog()//cette fonction pour return la page  blog
    {
        return $this->render('blog-main-2.html.twig');
    }

    /**
     * @Route("/contact" , methods="GET|POST",name="contact")
     */
    public function renderContact(EntityManagerInterface $em, Request $request) //cette fonction  est pour return la page de contact avec leur form
    {

        $contact=new Contact();//creation de neauveau entity de contact 

        $form=$this->createForm(ContactType::class,$contact);//creation  de nouveau form 

        $form->handleRequest($request);// permet a cette form de gerres les request 

        if($form->isSubmitted() && $form->isValid())//si les champs de form contact sont valide
        {
            $contact=$form->getData();//replir l'entity contact avec les donnes de la form contact 
            $em=$this->getDoctrine()->getManager();//creation  d'une entity manager pour gerres la communication avec la base de donnes
            $em->persist($contact);//eregistre l'entity contact
            $em->flush();//mettre a jour la base de donnees

            $contact=new Contact();//vider la form de contact 
            $form=$this->createForm(ContactType::class,$contact);//creation un nouveau form  vide apres l'envoie de la message


            $this->addFlash('success', "Votre message a bien été envoyé");//on stocke cette message on session 


            return $this->render('contacts.html.twig', [
                'form' => $form->createView()
            ]);//retun la page de contact avec la message de confirmation
        }


        return $this->render('contacts.html.twig', [
            'form' => $form->createView()
        ]);//return la page de contact avec la form 

    }

    /**
     * @Route("/reservation",methods="GET|POST",name="reservation-grid")
     */
    public function renderReservation(VehicleRepository $vehicle,EntityManagerInterface $em,PaginatorInterface $paginator, Request $request )//cette fonction pour passer les reservation 
    
    {

        $allVehicles=$vehicle->findAll();//obtenir tour les voiture

        $vehicles= $paginator->paginate(

            $allVehicles,

            $request->query->getInt('page', 1),

            5
        );//creation une pagination par un seule page qui contient 5 voiture 

        $reservation=new Reservation();//creation un nouveau instance de la entity reservation 
        $reservation->setStart(new \DateTime('now'));// initiatize l' interval de debut de la reservation
        $reservation->setStop(new \DateTime('now'));//initialize l'interval de fint de la reservation
        $form=$this->createForm(ReservationType::class,$reservation);//creation un form avec les input de reservation

    


        $form->handleRequest($request);//permet a cette form de gerres le request
        if($form->isSubmitted() && $form->isValid())//si les champs valide
        {

        
             $vehic=new Vehicle();//creation un nouveau entity de voiture 
            $vehic=$vehicle->find($this->session->get('idVehicle'));//obtenir id de la voiture qui entregistre en session
             $reservation->setVehicle($vehic);//associer la reservation par sa voiture 
             $reservation=$form->getData();//associer la reservation par leur donnes qui il sont ecrie en form
            $em->persist($reservation);//eneregistre la nouvelle  reservation 
            $em->flush();//mettre a jour la base de donnees
            $this->addFlash('success', "Votre réservation est confirmé");//stocke un messeage dans la session 

            return $this->render('reservation-grid.html.twig', [
                'vehicles' => $vehicles,'form' => $form->createView(),
            ]);//retun la page de reservation avec sa form 

        }



        return $this->render('reservation-grid.html.twig', [
            'vehicles' => $vehicles,'form' => $form->createView(),
        ]);//retun la page de reservation avec sa form 


    }

    /**
     * @Route("/sk/{id}" , methods="POST|GET",name="sk")
     */
    public function sk($id,EntityManagerInterface $em, Request $request)//cette fonction est pour enregistres id de la  voiture qui va le client a reserver 
    {

        $this->session->set('idVehicle', $id);
  


        return  '';
    }

    /**
     * @Route("/forum",name="forum")
     */
    public function renderForumMain()//return la page de forum
    {
        return $this->render('forum-main.html.twig');
    }

    /**
     * @Route("/vehicule-grille",name="inventory-grid")
     */
    public function renderInventoryGrid(VehicleRepository $vehicle,PaginatorInterface $paginator,Request $request)//cette fonction est pour return la page grid de l'affichage des voitures 
    {
         $allVehicles= $vehicle->findAll();//obtenir tour les voitures


        $vehicule_grille= $paginator->paginate(

            $allVehicles,

            $request->query->getInt('page', 1),

            5
        );//creation une pagination avec un seule page qui contient  5 voiture

        return $this->render('inventory-grid.html.twig', [
            'vehicule_grille' => $vehicule_grille
        ]);//return la page de layout grid
    }

    /**
     * @Route("/vehicule-liste",name="inventory-listings")
     */
    public function renderInventoryListings(VehicleRepository $vehicle)//cette fonction est pour return la page de list layout de l'affichage des voitures
    {
        $vehicule_liste = $vehicle->findAll();//obtenir tous les voitures

        return $this->render('inventory-listings.html.twig', [
            'vehicule_liste' => $vehicule_liste
        ]);//return la pages list layout des voitures
    }






    /**
     * @Route("/detail-vehicule/{id}",name="vehicle-details")
     */
    public function renderVehicleDetails($id, VehicleRepository $vehicle)//cette fonction est pour return une voiture specificque par leur id
    {
        $vehicule_detail = $vehicle->find($id);//obtenir la voiture par id

        return $this->render('vehicle-details.html.twig', [
            'vehicule_detail' => $vehicule_detail
        ]);//return la page de details voiture avec le voiture 
    }



    /**
     * @Route("/search",name="search", methods="POST")
     */
    public function renderSearch(Request $request, VehicleRepository $vehicle)//cette fonction pour le simple recherche par nom de la voiture
    {
        $array_search = ['name' => $request->get('name')];//le critaire de recherche 

        $vehicule_liste = $vehicle->findBy(
            $array_search,
            ['price' => 'ASC'],
            null,
            null
        );//obtenir le voiture qui contient les meme caracteristique

        return $this->render('inventory-listings.html.twig', [
            'vehicule_liste' => $vehicule_liste
        ]);//return les voiture a cherche en page list layout 
    }
}
